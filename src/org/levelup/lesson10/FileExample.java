package org.levelup.lesson10;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileExample {

    public static void main(String[] args) throws IOException {

        File universitiesFile = new File("universities.txt");
        System.out.println("Файл существует: " + universitiesFile.exists());

        // Для создания файла
        boolean wasCreated = universitiesFile.createNewFile();
        System.out.println("Был ли файл создан: " + wasCreated);
        System.out.println("Размер файл (байты): " + universitiesFile.length());

        String absolutePath = universitiesFile.getAbsolutePath();
        System.out.println("Абсолютный путь к файлу: " + absolutePath);

        System.out.println();

        File srcDir = new File("src");
        System.out.println("Папка src существует: " + srcDir.exists());
        System.out.println("Размер папки: " + srcDir.length());
        System.out.println("Размер папки: " + Files.readAttributes(Path.of(srcDir.getAbsolutePath()), "size"));

        System.out.println("Это папка: " + srcDir.isDirectory());

        // src/org/levelup/lesson11/package1
        File package1 = new File("src/org/levelup/lesson11/package1");
        boolean result = package1.mkdirs();
        System.out.println("Результат создания папок: " + result);

    }

}
