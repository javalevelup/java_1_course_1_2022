package org.levelup.lesson10;

@SuppressWarnings("ALL")
public class TryCatchFinally {

    // /Users/dmitryprotsko/IdeaProjects/level-up/java_1_course_1_2022/src/org/levelup/lesson10/TryCatchFinally.java
    // /src/org/levelup/lesson10/TryCatchFinally.java
    // /IdeaProjects/level-up/java_1_course_1_2022/src/org/levelup/lesson10/TryCatchFinally.java
    public static void main(String[] args) {
        int intVar = 10;
        int result = doSomething(intVar);
        System.out.println(result);
    }

    static int doSomething(int var) {
        try {
            if (var == 10) {
                throw new RuntimeException();
            } else {
                // System.exit(0); -> turn off JVM
                return 1;
            }
        } catch (Exception exc) {
            return 2;
        } finally {
            return 3;
        }
    }

}
