package org.levelup.lesson7.structures.abstractoverride;

public abstract class AC {

    public abstract void methodA();

    public abstract void methodB();

    public abstract void methodC();

}
