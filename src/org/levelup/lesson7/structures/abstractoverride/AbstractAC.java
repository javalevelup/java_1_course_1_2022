package org.levelup.lesson7.structures.abstractoverride;

public abstract class AbstractAC extends AC {

    @Override
    public void methodB() {

    }

    @Override
    public void methodC() {

    }

}
