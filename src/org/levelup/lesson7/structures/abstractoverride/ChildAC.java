package org.levelup.lesson7.structures.abstractoverride;

public class ChildAC extends AC {

    @Override
    public void methodA() {

    }

    @Override
    public void methodB() {

    }

    @Override
    public void methodC() {

    }

}
