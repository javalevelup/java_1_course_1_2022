package org.levelup.lesson7.structures;

public class Application {

    public static void main(String[] args) {

        int[] array = new int[10];
        array[0] = 34;
        array[1] = 36;
        array[2] = 0;

        //...


        DynamicArray dynamicArray = new DynamicArray();
        dynamicArray.add(34);
        dynamicArray.add(36);
        dynamicArray.add(0);
        dynamicArray.add(87);
        dynamicArray.add(26);
        dynamicArray.add(76);
        dynamicArray.add(1);
        // dynamicArray.deleteByIndex(2)

        OneWayList oneWayList = new OneWayList();
        oneWayList.add(34);
        oneWayList.add(54);
        oneWayList.add(42);
        oneWayList.add(75);
        oneWayList.add(54);
        oneWayList.add(56);
        oneWayList.add(23);

    }

}
