package org.levelup.lesson7.structures;

import java.util.List;

// Однонаправленный связный список
public class OneWayList extends AbstractList {

    private ListElement head;
    // private ListElement tail;
    // private int size;

    // add(57):
    //   el1(34) -> el2(54) -> el3(23)
    //   el4(57) - объект класса ListElement
    //   el1.next -> el2, el2.next -> el3, el3.next = null,
    //   el3.next = el4
    @Override
    public void add(int value) {
        ListElement el = new ListElement(value);
        if (head == null) {
            head = el;
        } else {
            ListElement cursor = head;
            while (cursor.getNext() != null) {
                cursor = cursor.getNext();
            }
            // cursor указывает на последний элемент списка
            cursor.setNext(el);
        }
        size++;
    }

    @Override
    public void remove(int value) {

    }

    @Override
    public void removeByIndex(int index) {
        ListElement prevCursor = null;
        ListElement cursor = head;
        int idx = 0;

        while (cursor.getNext() != null ){
            if (cursor.getValue() == 4) {
                // delete
                // prev.setNext(cursor.getNext());
                // cursor = cursor.getNext();
            } else {
                // prev = cursor;
                // cursor = cursor.getNext();
            }
        }
        // cursor - last
        // prev.setNext(null);

        while (idx != index) {
            idx++;
            prevCursor = cursor;
            cursor = cursor.getNext();
        }
        prevCursor.setNext(cursor.getNext());
    }

}
