package org.levelup.lesson7.structures;

public class StructureApplication {

    public static void main(String[] args) {

        AbstractList list = new OneWayList();
        addAndPrintCount(list);

        Structure s = new DynamicArray();
        s.add(45);
        s.add(23);
        s.add(12);
        s.add(Structure.MAX_CAPACITY);

    }

    static void addAndPrintCount(AbstractList list) {
        list.add(34);
        list.add(36);
        list.add(0);
        list.add(87);
        list.add(26);
        list.add(76);
        list.add(1);

        int[] array = {3, 4};
        list.addArray(array);
        System.out.println("List size: " + list.getSize());
    }

}
