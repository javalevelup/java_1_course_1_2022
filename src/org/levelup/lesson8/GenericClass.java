package org.levelup.lesson8;

// <T> - type alias
public class GenericClass<TYPE> {

    private TYPE field;
    private TYPE[] array;

    public TYPE getField() {
        return field;
    }

    public void setField(TYPE field) {
        this.field = field;
    }

    public void doSmth(TYPE[] array) {
        // TYPE[] array = new TYPE[3]; - нельзя)
        this.array = array;
    }

}
