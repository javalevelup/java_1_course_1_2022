package org.levelup.lesson8;

import org.levelup.lesson5.Product;

public class GenericClassApplication {

    public static void main(String[] args) {
        GenericClass genericClass = new GenericClass();
        genericClass.setField(new Product("A", "B", 34.23));
        // ...
        genericClass.setField(10);
        // ...
        genericClass.setField("Hello");

//        Object object = genericClass.getField();
//        Product p = (Product) object; // ClassCastException

        GenericClass<String> stringGC = new GenericClass<>();
        // stringGC.setField(new Product("A", "B", 45.34));
        stringGC.setField("Hello");

        GenericClass<Product> productGC = new GenericClass<>();
        productGC.setField(new Product("A", "B", 45.23));

        GenericClass rawTypeGC = new GenericClass();
        rawTypeGC.setField(new Object());

        GenericClass<String> stringGenericClass = new GenericClass<>();
        stringGenericClass.setField("Hello again");
        String field = stringGenericClass.getField();

        String[] array = {"", "1", "2"};
        stringGenericClass.doSmth(array);


    }

}
