package org.levelup.lesson8;

import java.util.ArrayList;
import java.util.List;

public class CollectionApp {

    public static void main(String[] args) {

        List<String> universityNames = new ArrayList<>(); // ~ DynamicArray

        universityNames.add("СПбГУ");
        universityNames.add("ИНЖЭКОН");
        universityNames.add("СПбГПУ");
        universityNames.add("ПГУПС");

        // for (<type> <var_name> : <collection> )
        // ConcurrentModificationException
        for (String name : universityNames) {
            System.out.println(name);
        }

        System.out.println("Вуз ЛЭТИ есть ли в коллекции: " + universityNames.contains("ЛЭТИ"));
        System.out.println("Под индексом 2: " + universityNames.get(2));

    }

}
