package org.levelup.lesson4;

// superclass, baseclass
public class Shape {

    // значения длин сторон фигуры
    int[] sides;

//    Shape() {
//        System.out.println("Execute Shape constructor");
//    }

    Shape(int[] sides) {
        this.sides = sides;
    }

    double perimeter() {
        System.out.println("Shape#perimeter()");
        double sum = 0;
        for (int i = 0; i < sides.length; i++) {
            sum = sum + sides[i];
        }
        return sum;
    }

}
