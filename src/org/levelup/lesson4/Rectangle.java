package org.levelup.lesson4;

// subclass
public class Rectangle extends Shape {

    // int[] sides;

    Rectangle() {
        super(new int[0]); // создали пустой массив (в нем 0 элементов) и передали в конструктор класса Shape(int[])
        System.out.println("Execute Rectangle constructor");
    }

    Rectangle(int length, int width) {
        // super();
        // super.sides = new int[] {length, width, length, width };
        super(new int[] {length, width, length, width });
        // sides = new int[] {length, width, length, width };
        // this.sides = new int[] {length, width, length, width };
    }

    @Override
    double perimeter() {
        System.out.println("Rectangle#perimeter()");
        return sides[0] * 2 + sides[1] * 2;
    }

    double square() {
        return 0;
    }

//    double perimeter() {
//        double sum = 0;
//        for (int i = 0; i < sides.length; i++) {
//            sum = sum + sides[i];
//        }
//        return sum;
//    }

}
