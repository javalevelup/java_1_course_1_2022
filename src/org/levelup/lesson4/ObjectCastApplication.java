package org.levelup.lesson4;

@SuppressWarnings("ALL")
public class ObjectCastApplication {

    public static void main(String[] args) {

        Rectangle r = new Rectangle(3, 5);
        // r.square();
        Shape s = r; // неявное расширяющее преобразование
        // s.square(); - нельзя вызвать
        // Object o = s;
        Object o = r;  // неявное расширяющее преобразование
        // o.perimeter();

        Shape so = (Shape) o; // явное сужающее преобразование
        Rectangle rs = (Rectangle) so; // явное сужающее преобразование

        //
        // Rectangle r = new Rectangle(4, 6);
        // Shape shape = r;
        Shape shape = new Rectangle(4, 6);
        Shape sh = new Shape(new int[] {2, 3, 5, 7, 8});

        System.out.println(shape.perimeter());
        System.out.println(sh.perimeter());
        // Rectangle rsh = (Rectangle) sh; // produces ClassCastException

    }

}
