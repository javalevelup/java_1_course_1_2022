package org.levelup.lesson4;

public class ShapeStorage {

    // Shape[] shapes;
    // Rectangle[] rectangles;
    // Triangle[] triangles;

    // add(Shape s) -> shapes[i]
    // add(Rectangle s) -> rectangle[i]
    // add(Triangle s) -> triangle[i]

    // printAllPerimeters(Shape[] shapes)
    // printAllPerimeters(Rectangle[] shapes)
    // printAllPerimeters(Triangle[] shapes)

    Shape[] shapes;
    int index;

    ShapeStorage() {
        // null
        this.shapes = new Shape[10];
        // [null, null, null, null, null, null, null, null, null, null]
        this.index = 0;
    }

    void addShape(Shape shape) {
        // [sh1, r1, r2, t1, null, null, null, null, null, null]
        shapes[index] = shape;
        index = index + 1;
    }

    void printPerimeters() {
        for (int i = 0; i < shapes.length; i++) { // for (int i = 0; i < index; i++) {}
            if (shapes[i] != null) {
                // shapes[i] -> Shape
                double p = shapes[i].perimeter();
                System.out.println(p);
                System.out.println();
            }
        }
    }

}
