package org.levelup.lesson1;

// Full class name: org.levelup.lesson1.PrimitiveTypes
public class PrimitiveTypes {

    // 01101 -> 1 * 2^0 + 0 * 2^1 + 1 * 2^2 + 1 * 2^3 + 0 * 2^4 = 13

    // psvm + Tab/Enter -> public static void main(String[] args) {}
    // sout + Tab/Enter -> System.out.println();
    public static void main(String[] args) {

        int a;
        a = 156;

        int b = 756;

        int sum = a + b;
        System.out.println(sum);

        char symbol = 'H';
        char intSymbol = 72;
        System.out.println(symbol);
        System.out.println(intSymbol);

        float f1 = 543.34f;
        float f2 = 234.23f;

        System.out.println(f1 + f2);

        // "Sum " + "543.34" -> "Sum 543.34"
        System.out.println("Sum " + f1 + f2);
        System.out.println("Sum " + (f1 + f2));

        // sout + Tab/Enter -> System.out.println();

    }

}
