package org.levelup.lesson2;

public class Loops {

    public static void main(String[] args) {

        for (int digit = 0; digit < 20; digit++) {
            // 4 - 16
            // 5 - 25
            System.out.println(digit + " - " + (digit * digit));
        }

//        for (int i = 0; i < 10; i++) {
//            for (int j = 0; j < 10; j++) {
//                System.out.println(i + " " + j);
//            }
//        }

        int digit = 0;
        while (digit * digit * digit < 1000) {
            // Math.pow() - возведение в степень
            // Math.sqrt(digit) / Math.pow(digit, 0.5) - квадратный корень числа
            System.out.println(digit + " - " + (int) Math.pow(digit, 3));
            digit = digit + 1;
        }


        int d = 0;
        int cube = d * d * d;
        do {
            System.out.println(d + " - " + cube);
            d++;
            cube = d * d * d;
        } while (cube < 1000);


    }

}
