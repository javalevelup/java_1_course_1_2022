package org.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) {
        // 1. Будем использовать ГПСЧ
        // 2. Считывать ввод с консоли

        Random generator = new Random(); // инициализируем переменную generator
        // generator создаст ПС число и сохранит в переменную secret
        // nextInt() -> [0, Integer.MAX_VALUE)
        // nextInt(4) -> [0, 3]

        // [10; 20] -> .nextInt(11) + 10;
        // nextInt(11) + 10 -> [0; 10] + 10 -> [10; 20];

        // [-30; -17] -> nextInt(14) - 30;
        // nextInt(14) - 30 -> [0; 13] - 30 -> [-30; -17];
        int secret = generator.nextInt(4);

        // Ввод с консоли
        System.out.println("Введите число:");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();

        // if .. else
        // if (condition) { ... } else { ... }

        // boolean condition = number == secret; // true/false
//        if (number == secret) {
//            System.out.println("Вы угадали!");
//        } else {
//            System.out.println("Вы ввели неправильное число");
//            if (number < secret) {
//                System.out.println("Вы ввели число меньше, чем загаданное");
//            } else {
//                System.out.println("Вы ввели число больше, чем загаданное");
//            }
//        }

        if (number == secret) {
            System.out.println("Вы угадали!");
        } else if (number < secret) {
            System.out.println("Вы ввели неправильное число");
            System.out.println("Вы ввели число меньше, чем загаданное: " + number);
        } else {
            System.out.println("Вы ввели неправильное число");
            System.out.println("Вы ввели число больше, чем загаданное: " + number);
        }


        System.out.println(secret);
        System.out.println(number);
    }

}


