package org.levelup.lesson2;

public class Arithmetic {

    // psvm + Tab/Enter
    public static void main(String[] args) {
        int a = 35;
        int b = 4;

        int div = a / b;
        System.out.println(div);

        double doubleDiv = a / b;
        System.out.println(doubleDiv);

        long longVar = a; // int -> long - преобразование типов (приведение типов) - неявное расширяющее
        // (type)
        int intVar = (int) longVar; // явное сужающее

        double division = (double) a / b;
        // double division =  a / (double) b;
        System.out.println(division);

        // [-128; 127]
        byte bigByte = (byte) 128; // -128
        // byte bigByte = (byte) 129; // -127
        // byte bigByte = (byte) 130; // -126
        // byte bigByte = (byte) 131; // -125

        // byte bigByte = (byte) -129; // 127
        // byte bigByte = (byte) -130; // 126
        // byte bigByte = (byte) -131; // 125

        System.out.println(bigByte);

/*
        int bigInt = (int) (Integer.MAX_VALUE + 1);
        System.out.println(bigInt);
*/
    }

}
