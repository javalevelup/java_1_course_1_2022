package org.levelup.lesson6;

public class UniversityInMemoryStorage {

    private University[] universities;

    public UniversityInMemoryStorage() {
        this.universities = new University[10];
        this.universities[0] = new University("СПбГУ" , 1784);
        this.universities[1] = new University("СПбГПУ" , 1808);
        this.universities[2] = new University("ПГУПС" , 1801);
    }

    public boolean exist(University university) {
        for (int i = 0; i < universities.length; i++) {
            if (universities[i] != null) {
                if (university.hashCode() == universities[i].hashCode() && university.equals(universities[i])) {
                    return true;
                }
            }
        }
        // Университет не найден
        return false;
    }

}
