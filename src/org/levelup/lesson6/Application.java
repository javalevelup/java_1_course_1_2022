package org.levelup.lesson6;

public class Application {

    public static void main(String[] args) {
        University existUniversity = new University("ПГУПС", 1801);
        University nonExistUniversity = new University("ВШЭ", 1904);

        UniversityInMemoryStorage inMemoryStorage = new UniversityInMemoryStorage();

        boolean exist = inMemoryStorage.exist(existUniversity);
        System.out.println("ПГУПС существует: " + exist);

        exist = inMemoryStorage.exist(nonExistUniversity);
        System.out.println("ВШЭ существует: " + exist);

        //
        InMemoryStorage stringStorage = new InMemoryStorage();
        stringStorage.addToArray("Product");
        stringStorage.addToArray("Phone");
        stringStorage.addToArray("Train");

        System.out.println("Phone exist: " + stringStorage.exist("Phone"));
        System.out.println("Pencil exist: " + stringStorage.exist("Pencil"));

        InMemoryStorage universityStorage = new InMemoryStorage();
        universityStorage.addToArray(new University("СПбГУ" , 1784));
        universityStorage.addToArray(new University("ЛЭТИ"  , 1867));
        universityStorage.addToArray(new University("ИНЖЭКОН"  , 1943));
        universityStorage.addToArray(new University("ПГУПС", 1801));

        System.out.println("ПГУПС существует: " + universityStorage.exist(existUniversity));
        System.out.println("ВШЭ существует: " + universityStorage.exist(nonExistUniversity));


    }

}
