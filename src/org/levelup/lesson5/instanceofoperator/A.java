package org.levelup.lesson5.instanceofoperator;

import java.util.Objects;

public class A {

    String test;

    @Override
    public boolean equals(Object obj) {
//        if (!(obj instanceof A)) return false;
        if (obj == null || getClass() != obj.getClass()) return false;

        return true;
    }

}
