package org.levelup.lesson5.instanceofoperator;

public class ABTest {

    public static void main(String[] args) {
        A a = new A();
        B b = new B();

        System.out.println("a.equals(b): " + a.equals(b));
        System.out.println("b.equals(a): " + b.equals(a));
    }

}
