package org.levelup.lesson5.instanceofoperator;

public class B extends A {

    @Override
    public boolean equals(Object obj) {
        // if (!(object instanceof B)) return false;
        if (obj == null || getClass() != obj.getClass()) return false;
        return true;
    }

}
