package org.levelup.lesson5;

import java.util.Objects;

// Access modifiers (модификаторы доступа)
// 1. private - доступ к полю/вызов метода возможен только внутри класса
// 2. default-package (private-package) - private + доступ внутри пакета
// 3. protected - default-package + доступ из класса-наследника (даже если они находятся в других пакетах)
// 4. public - доступ отовсюду
public class Product {

    private String name;
    private String vendorCode;
    private double price;

    public Product(String name, String vendorCode, double price) {
        this.name = name;
        this.vendorCode = vendorCode;
        this.price = price;
    }

    // getter/setter
    // setter
    // public void set<Field_name>(<Field_type> <field_name>) {
    //      this.<field_name> = <field_name>;
    // }
    public void setName(String name) {
        this.name = name;
    }

    // getter
    // public <Field_type> get<Field_name>() {
    //  return <field_name>;
    // }
    public String getName() {
        return name;
    }

    // p2 -> object (Product -> Object)
    // object -> o (Object -> Product)

    // p1.equals("S") -> object.getClass() -> String.class
    @Override
    public boolean equals(Object object) {
        // this
        // object
        if (this == object) return true;

        // 1 способ (подходит для классов, у которых нет наследников и нет родителей)
        // instanceof
        // <переменная ссылочного типа> instanceof <имя класса> -> можно ли переменную привести к типу?
        // null instanceof <любой класс> -> false
        // if (!(object instanceof Product)) return false;

        // 2 способ (подходит для всех случаев)
        if (object == null || this.getClass() != object.getClass()) return false;

        Product other = (Product) object;
        // < 0 -> d1 < d2
        // = 0 -> d1 = d2
        // > 0 -> d1 > d2
        // Double.compare(d1, d2) == 0 -> true/false

        // this.vendorCode = null;
        // other.vendorCode = null;
        // (vendorCode == other.vendorCode || (vendorCode != null && vendorCode.equals(other.vendorCode)))
        return Objects.equals(vendorCode, other.vendorCode) && Double.compare(price, other.price) == 0;
//        if (vendorCode.equals(other.vendorCode) && Double.compare(price, other.price) == 0) {
//            return true;
//        }
//        return false;

//        return vendorCode.equals(other.vendorCode) && Double.compare(price, other.price) == 0;
    }

}
