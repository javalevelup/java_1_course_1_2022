package org.levelup.lesson5;

import java.util.Properties;

@SuppressWarnings("ALL")
public class Application {

    public static void main(String[] args) {
        Product p1 = new Product("Lenovo A534", "LA543", 23543.34); // abc234
        Product p2 = new Product("Lenovo A534", "LA543", 23543.34); // abc236
        Product p3 = p1;  // abc234

        boolean isEqual = p1 == p2; // false

        // p2.equals(p1)
        // p2 -> this
        // p1 -> object

        // p1 -> this
        // p2 -> object
        boolean areObjectsEqual = p1.equals(p2); // false -> p1 == p2
        System.out.println("p1.equals(p2): " + areObjectsEqual);


//        p1.equals("String");
//        Product p4 = null;
//        p1.equals(null);
//        p1.equals(p4);
        // ...
        // product.setName("Phone2");
        // System.out.println(product.getName());


        // product.name = "Phone";
        // String name = product.name;
    }

}
