package org.levelup.hw;

public class Constructors {

    long a;
    long b;
    int c;

    public Constructors(long a) {
        // this(a, 0L);
        this.a = a;
        this.b = 0;
        this.c = 0;
    }

    public Constructors(int c) {
        this(0L, 0L, c);
    }

    public Constructors(long a, long b) {
        this(a, b, 0);
    }

    public Constructors(long a, long b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

}
