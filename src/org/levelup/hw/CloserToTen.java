package org.levelup.hw;

public class CloserToTen {

    public static void main(String[] args) {

        int n = 12;
        int m = 9;

        int diff1 = Math.abs(n - 10); // 2
        int diff2 = Math.abs(m - 10); // -1

        if (diff1 < 0) {
            diff1 = -diff1;
        }

        if (diff2 < 0) {
            diff2 = -diff2;
        }


    }

}
