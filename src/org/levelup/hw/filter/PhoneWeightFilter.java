package org.levelup.hw.filter;

public class PhoneWeightFilter extends BaseFilter {

    @Override
    public boolean filter(Object object) {
        if (!(object instanceof Phone)) return false;

        Phone phone = (Phone) object;
        return phone.getWeight() > 100;
    }

}
