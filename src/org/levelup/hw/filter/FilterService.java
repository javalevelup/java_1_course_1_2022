package org.levelup.hw.filter;

public class FilterService {

    public Object[] filterArray(Object[] original, BaseFilter baseFilter) {
        Object[] filteredArray = new Object[original.length];
        int filterArrayIndex = 0;

        for (int i = 0; i < original.length; i++) {
            if (baseFilter.filter(original[i])) {
                filteredArray[filterArrayIndex] = original[i];
                filterArrayIndex++;
            }
        }

        // [2, 4, 5, null, null, null]
        return filteredArray;
    }

}
