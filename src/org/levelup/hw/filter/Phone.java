package org.levelup.hw.filter;

public class Phone {

    private String brand;
    private String model;
    private double weight;

    public Phone(String brand, String model, double weight) {
        this.brand = brand;
        this.model = model;
        this.weight = weight;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", weight=" + weight +
                '}';
    }

}
