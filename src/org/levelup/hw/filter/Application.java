package org.levelup.hw.filter;

public class Application {

    public static void main(String[] args) {
        Phone[] phones = {
                new Phone("A", "B", 102),
                new Phone("B", "C", 92),
                new Phone("C", "D", 90),
                new Phone("D", "E", 345),
        };

        FilterService filterService = new FilterService();
        Object[] filtered = filterService.filterArray(phones, new PhoneWeightFilter());

        for (int i = 0; i < filtered.length; i++) {
            if (filtered[i] != null) {
                System.out.println(filtered[i]);
            }
        }
    }

}
