package org.levelup.lesson9.exceptions;

public class Stack<T> {

    private Object[] array;
    private int elementsCount;

    public Stack(int stackSize) {
        this.array = new Object[stackSize];
    }

    // put - добавляет элемент в стэк
    // pop - забирает элемент с вершины стека

    public void put(T value) throws StackOverflowException {
        // Проверяем заполненность стэка
        if (elementsCount == array.length) {
            throw new StackOverflowException(array.length);
        }
        array[elementsCount++] = value; // array[elementsCount] = value; elementsCount++;
    }

    public T pop() {
        if (elementsCount == 0) { // стэк пустой
            // Выбросить/Сгенерировать исключение - throw
//            EmptyStackException exc = new EmptyStackException();
//            throw exc;
            // throw new RuntimeException("ВСЕ ПЛОХООО!");
            throw new EmptyStackException();
        }

        return (T) array[--elementsCount]; // elementsCount = elementsCount - 1; return array[elementsCount];
    }

}
