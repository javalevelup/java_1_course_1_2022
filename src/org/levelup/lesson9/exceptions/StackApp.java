package org.levelup.lesson9.exceptions;

public class StackApp {

    public static void main(String[] args) { //throws StackOverflowException {

        Stack<String> todolist = new Stack<>(0);
        // todolist.pop();

        // try-catch-finally
        // try-catch
        // try-catch-catch-catch...
        // try-finally
        try {
            todolist.put("Check emails");
            todolist.pop();
            todolist.pop();
        } catch (EmptyStackException exc) {
            System.out.println("All TODOs have been completed");
        } catch (StackOverflowException exc) {
            System.out.println("We can't do it today");
            String message = exc.getMessage();
            System.out.println("Message: " + message);
        } finally {
            System.out.println("Always!");
        }
        System.out.println("Program finished");
    }

}
