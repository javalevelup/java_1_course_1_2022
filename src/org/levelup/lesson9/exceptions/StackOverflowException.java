package org.levelup.lesson9.exceptions;

// checked
public class StackOverflowException extends Exception {

    private int stackSize;

    public StackOverflowException(int stackSize) {
        super("Stack is full. Stack size: " + stackSize);
        this.stackSize = stackSize;
    }

    public int getStackSize() {
        return stackSize;
    }

}
