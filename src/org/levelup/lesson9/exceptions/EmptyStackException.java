package org.levelup.lesson9.exceptions;

// unchecked
public class EmptyStackException extends RuntimeException {

    public EmptyStackException() {
        super("Stack is empty");
    }

}
