package org.levelup.lesson9;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Maps {

    public static void main(String[] args) {

        Map<String, Integer> products = new HashMap<>(100);
        // Map<String, Collection<Integer>> -> Set<Map.Entry<String, Collection<Integer>>>

        products.put("Milk", 10);
        products.put("Bread", 15);
        products.put("Cheese", 3);
        products.put("Chocolate", 5);
        products.put("Tea", 12);

        Integer countOfCheese = products.get("Cheese");
        System.out.println("Count of cheese: " + countOfCheese);

        Integer countOfCoffee = products.get("Coffee");
        System.out.println("Count of coffee: " + countOfCoffee);

        System.out.println();
        // 1 variant
        Set<String> keys = products.keySet(); // Set<String> -> ["Milk", "Bread", "Cheese","Chocolate", "Tea"]
        for (String key : keys) {
            Integer value = products.get(key);
            System.out.println("Key: " + key +", value: " + value);
        }

        // 2 variant
        // OuterClass.InnerClass -> Map.Entry
        products.put("Milk", 20);
        System.out.println();
        Set<Map.Entry<String, Integer>> entries = products.entrySet(); // [ {"Milk"-10}, {"Bread"-15}, ...  ]
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println("Key: " + entry.getKey() + ", value: " + entry.getValue());
        }

    }

}
