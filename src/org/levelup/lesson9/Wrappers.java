package org.levelup.lesson9;

@SuppressWarnings("ALL")
public class Wrappers {

    public static void main(String[] args) {
        // long - Long
        // double - Double
        // int - Integer
        // char - Character

        Long l = 203L;
        Integer integer = null;

        // boxing
        // primitive -> Wrapper (int -> Integer)
        Integer i = Integer.valueOf(10);
        // unboxing
        // Wrapper -> primitive
        long lon = l.longValue();

        // autoboxing/autounboxing
        Double d = 353.23d;     // autoboxing
        double pd = d;          // autounboxing

        Float f = null;
        // float pf = f; // float pf = f.floatValue(); -> NullPointerException

        Integer i1 = 6434;
        Integer i2 = 6434;
        System.out.println(6434 == i1);

    }

}
