package org.levelup.universities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleReader {

    public static void main(String[] args) throws IOException {

        // System.in    - InputStream
        // System.out   - OutputStream
        // System.err   - OutputStream
        // new Scanner(System.in);
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter line:");
        String line = consoleReader.readLine();
        System.out.println("You entered: " + line);

        System.out.println("Enter number:");
        int number = Integer.parseInt(consoleReader.readLine());
        System.out.println(number);

    }

}
