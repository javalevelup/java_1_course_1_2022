package org.levelup.universities;

import java.util.List;

public class UniversitiesApp {

    public static void main(String[] args) {
        UniversitiesFileService fs = new UniversitiesFileService();

        University university = new University("СПбГПУ", 1865);
        fs.write("universities.txt", university);

        List<University> universities = fs.read("universities.txt");
        for (University u : universities) {
            System.out.println(u.getName() + " " + u.getFoundationYear());
        }

    }

}
