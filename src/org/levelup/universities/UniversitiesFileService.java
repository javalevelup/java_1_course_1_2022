package org.levelup.universities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UniversitiesFileService {

    // Reader
    // Writer
    // InputStream
    // OutputStream

    public List<University> read(String filepath) {
        List<University> universities = new ArrayList<>();
        // try-with-resources
        try (BufferedReader buffReader = new BufferedReader(new FileReader(filepath))) {
            String line = buffReader.readLine(); // читаем и пропускаем строчку с заголовками
            // (line = buffReader.readLine()) != null
            // 1. read line from file
            // 2. write value (String) to variable "line"
            // 3. line compares with null
            while ((line = buffReader.readLine()) != null) {
                if (!line.isBlank()) { // !line.trim().isEmpty()
                    University university = parseUniversity(line);
                    universities.add(university);
                }
            }
        } catch (FileNotFoundException exc) {
            System.out.println("Файл с таким названием/путем не найден: " + filepath);
        } catch (IOException exc) {
            System.out.println("Ошибка чтения файла: " + exc.getMessage());
        }

        return universities;
    }

    public void write(String filepath, University university) {
        try (BufferedWriter buffWriter = new BufferedWriter(new FileWriter(filepath, true))) {
            buffWriter.newLine(); // adds '\n'

            String line = university.getName() + "," + university.getFoundationYear();
            buffWriter.write(line);

        } catch (IOException exc) {
            System.out.println("Ошибка записи в файл: " + exc.getMessage());
        }
    }

    private University parseUniversity(String line) {
        // "СПбГУ,1890"
        String[] split = line.split(","); // "СПбГУ,1890" -> ["СПбГУ", "1890"]
        return new University(
                split[0],
                Integer.parseInt(split[1].trim())   // "   234 ".trim() -> "234"; , "     "
        );
    }

}
