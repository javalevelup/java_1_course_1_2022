package org.levelup.lesson3;

public class Application {

    public static void main(String[] args) {

//        int x1 = 40;
//        int y1 = 45;
//
//        int x2 = 43;
//        int y2 = 52;
//
//        int[] xCoordinates;
//        int[] yCoordinates;

        // p1/p2 - переменная ссылочного типа - объект
        // объект, экземпляр, ссылка, object, instance, reference
        Point p1 = new Point();
        System.out.println("p1 before setting values (" + p1.x + ", " + p1.y + ")");
        p1.x = 4;
        p1.y = 45;
        // String name = "A";
        // p1.name = name;
        p1.name = "A"; // p1.name = new String("A");
        System.out.println("p1 - " + p1.name + "(" + p1.x + ", " + p1.y + ")");

        p1.changePoint(12, 34);
        System.out.println("p1 - " + p1.name + "(" + p1.x + ", " + p1.y + ")");
        p1.changePoint(-45, 32);
        System.out.println("p1 - " + p1.name + "(" + p1.x + ", " + p1.y + ")");


        int p1Quadrant = p1.quadrant();
        System.out.println("P1 quadrant - " + p1Quadrant);


        p1.changePoint("B", 3, 4);

        p1.printPoint();

        Point p2 = new Point("C", 54, -12);
        Point p3 = new Point(-23, -34);

    }

}
