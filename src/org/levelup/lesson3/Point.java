package org.levelup.lesson3;

// pointCalculator.distance(p1, p2)
// p1.distance(p1, p2);

// p1.distance(p2);
public class Point {

    // Поле класса (class field)
    int x;
    int y;
    String name;

    // конструктор по-умолчанию
    public Point() {

    }

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Point(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    // <тип возвращаемого значения> <имя метода>(<тип аргумента1> <имя аргумента1>, <тип аргумента2> <имя аргумента2>) {}
    // void - нет возращаемого значения

    void changePoint(int newX, int newY) {
        x = newX;
        // return;
        y = newY;
    }

    // Сигнатура метода - имя метода + список аргументов (тип и порядок аргументов)
    // В одном классе не может быть двух методов с одинаковой сигнатурой

    // Overloading - перегрузка методов - позволяет описывать методы с одинаковым названием, но разным набором аргументов
    void changePoint(String name, int x, int y) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

    // int m(int a, double b) - m(int, double)

    // 1. int m(double b, int a)    - yes - m(double, int)
    // 2. int m(double a, int b)    - yes - m(double, int)
    // 3. int m(int b, double a)    - no  - m(int, double)
    // 4. int m(int f, int d)       - yes - m(int, int)
    // 5. double m(int a, double b) - no  - m(int, double)
    // 6. int m(int a)              - yes - m(int)
    // 7. int m(int a, double b, int a) - yes - m(int, double, int)



    // 1. m(long l);
    // 2. m(double d);

    // int a = 2; m(a); - m(long)
    // float f = 3.4; m(f); - m(double)
    // byte b = 4; m(b); - m(long)

    void printPoint() {
        System.out.println(this.name + "(" + this.x + ", " + this.y + ")");
    }

    //
    int quadrant() {
        if (x == 0 && y == 0) {
            return 0;
        }

        if (x > 0 && y > 0) {
            return 1;
        } else if (x > 0 && y < 0) {
            return 4;
        } else if (x < 0 && y > 0) {
            return 2;
        } else {
            return 3;
        }

//        int result = 0;
//        if (x > 0 && y > 0) {
//            result = 1;
//        } else if (x > 0 && y < 0) {
//            result = 4;
//        } else if (x < 0 && y > 0) {
//            result = 2;
//        } else {
//            result = 3;
//        }
//        return result;

//        int quadrant = 0;
//        return quadrant; // return 0;
    }

}
